package com.leboncoin.steventbtest.model.photo;

import com.leboncoin.steventbtest.model.entity.Photo;
import com.leboncoin.steventbtest.model.network.Network;
import com.leboncoin.steventbtest.model.network.NetworkCallback;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by Steven on 15/02/2018.
 */

public class PhotoClientTest {

    private Network mNetwork;

    @Test
    public void testError() throws Exception {
        // Given : network will fail
        mNetwork = new Network() {
            @Override
            public void get(NetworkCallback callback) {
                callback.failure("{\"error\":\"lol\"}");
            }
        };
        PhotoClient client = new PhotoClient(mNetwork);

        // When :
        final boolean[] failureCalled = {false};
        client.getList(new PhotoRepository.PhotoListListener() {
            @Override
            public void onPhotoListReceived(List<Photo> photoList) {

            }

            @Override
            public void onSuccess(String msg) {
            }

            @Override
            public void onFailure(String msg) {
                failureCalled[0] = true;
            }
        });
        
        // Then :
        Assert.assertTrue(failureCalled[0]);
    }
}
