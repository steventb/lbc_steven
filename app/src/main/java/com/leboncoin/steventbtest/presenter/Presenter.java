package com.leboncoin.steventbtest.presenter;

/**
 * Created by Steven on 11/02/2018.
 */

public interface Presenter {
    void init();
    void destroy();
}
