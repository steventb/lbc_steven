package com.leboncoin.steventbtest.presenter;

import com.leboncoin.steventbtest.model.entity.Photo;
import com.leboncoin.steventbtest.model.photo.PhotoRepository;
import com.leboncoin.steventbtest.view.PhotoListView;

import java.util.List;

/**
 * Created by Steven on 11/02/2018.
 */

public class PhotoListPresenter implements Presenter, PhotoRepository.PhotoListListener {

    private PhotoListView mView;
    private PhotoRepository mPhotoRepository;

    public PhotoListPresenter(PhotoListView view, PhotoRepository photoRepository){
        mView = view;
        mPhotoRepository = photoRepository;

    }

    // Presenter
    @Override
    public void init() {
        mView.showProgressBar();
        mView.hideRetryLayout();
        mPhotoRepository.getList(this);
    }

    @Override
    public void destroy() {
        mView = null;
        mPhotoRepository = null;
    }

    // Model callbacks
    @Override
    public void onPhotoListReceived(List<Photo> photoList) {
        mView.hideProgressBar();
        mView.hideRetryLayout();
        mView.renderPhotoList(photoList);
    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onFailure(String msg) {
        mView.hideProgressBar();
        mView.showRetryLayout();
    }
}
