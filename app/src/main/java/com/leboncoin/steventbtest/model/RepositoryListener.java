package com.leboncoin.steventbtest.model;

/**
 * Created by Steven on 11/02/2018.
 */

public interface RepositoryListener {
    void onSuccess(String msg);
    void onFailure(String msg);
}
