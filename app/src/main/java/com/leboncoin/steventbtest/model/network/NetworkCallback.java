package com.leboncoin.steventbtest.model.network;

/**
 * Created by Steven on 11/02/2018.
 */

public interface NetworkCallback {
    void success(String body);
    void failure(String body);
}
