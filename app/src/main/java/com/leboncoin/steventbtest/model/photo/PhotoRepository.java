package com.leboncoin.steventbtest.model.photo;

import com.leboncoin.steventbtest.model.RepositoryListener;
import com.leboncoin.steventbtest.model.entity.Photo;

import java.util.List;

/**
 * Created by Steven on 11/02/2018.
 */

public class PhotoRepository {
    private PhotoClient mClient;

    public PhotoRepository(PhotoClient client) {
        mClient = client;
    }

    public void getList(final PhotoListListener callback){
        PhotoListListener listener = new PhotoListListener() {
            @Override
            public void onPhotoListReceived(List<Photo> photos) {
                // Return response to callback
                callback.onPhotoListReceived(photos);
            }

            @Override
            public void onFailure(String msg) {
                callback.onFailure(msg);
            }

            @Override
            public void onSuccess(String msg) {
                callback.onSuccess(msg);
            }
        };

        mClient.getList(listener);
    }

    public interface PhotoListListener extends RepositoryListener {
        void onPhotoListReceived(List<Photo> photoList);
    }

}
