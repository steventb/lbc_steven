package com.leboncoin.steventbtest.model.photo;

import com.leboncoin.steventbtest.model.network.Network;
import com.leboncoin.steventbtest.model.network.NetworkCallback;

/**
 * Created by Steven on 11/02/2018.
 */

public abstract class BaseClient {

    private Network mNetwork;

    BaseClient(Network network) {
        mNetwork = network;
    }

    public void makeGetRequest(NetworkCallback callback) {
        mNetwork.get(callback);
    }
}
