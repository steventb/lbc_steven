package com.leboncoin.steventbtest.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Steven on 11/02/2018.
 */

public class Photo {
    @SerializedName("albumId")
    public int albumId;

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("url")
    public String url;

    @SerializedName("thumbnailUrl")
    public String thumbUrl;
}
