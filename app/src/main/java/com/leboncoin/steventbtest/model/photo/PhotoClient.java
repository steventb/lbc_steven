package com.leboncoin.steventbtest.model.photo;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.leboncoin.steventbtest.model.entity.Photo;
import com.leboncoin.steventbtest.model.network.Network;
import com.leboncoin.steventbtest.model.network.NetworkCallback;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steven on 11/02/2018.
 */

public class PhotoClient extends BaseClient {

    public PhotoClient(Network network) {
        super(network);
    }

    public void getList(final PhotoRepository.PhotoListListener callback){
        NetworkCallback networkCallback = new NetworkCallback() {
            @Override
            public void success(String body) {
                Type empTypeList = new TypeToken<ArrayList<Photo>>(){}.getType();
                List<Photo> photoList = new Gson().fromJson(body, empTypeList);
                callback.onPhotoListReceived(photoList);
            }

            @Override
            public void failure(String body) {
                // TODO: set default error message
                String message = "";
                // At first I used TextUtils but I need to split that in order to run UT in PhotoClientTest
                if (null != body && !body.isEmpty()) {
                    Error error = new Gson().fromJson(body, Error.class);
                    message = error.getMessage();
                }
                callback.onFailure(message);
            }
        };

        makeGetRequest(networkCallback);
    }

}
