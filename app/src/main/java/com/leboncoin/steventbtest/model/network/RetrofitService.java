package com.leboncoin.steventbtest.model.network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Steven on 11/02/2018.
 */

public interface RetrofitService {
    @GET("photos/")
    Call<ResponseBody> getPhotos();
}
