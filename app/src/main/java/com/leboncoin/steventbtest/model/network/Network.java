package com.leboncoin.steventbtest.model.network;

/**
 * Created by Steven on 11/02/2018.
 */

public interface Network {
    void get(final NetworkCallback callback);
}