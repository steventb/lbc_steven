package com.leboncoin.steventbtest.model;

/**
 * Created by Steven on 11/02/2018.
 */

public final class Environment {
    // The base url we have to target
    // Note that it would be useful to change the path in the Network impl
    public static final String ENDPOINT = "http://jsonplaceholder.typicode.com";
}
