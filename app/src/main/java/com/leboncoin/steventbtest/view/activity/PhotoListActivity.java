package com.leboncoin.steventbtest.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.leboncoin.steventbtest.R;
import com.leboncoin.steventbtest.model.entity.Photo;
import com.leboncoin.steventbtest.presenter.PhotoListPresenter;
import com.leboncoin.steventbtest.view.BaseApplication;
import com.leboncoin.steventbtest.view.PhotoListView;
import com.leboncoin.steventbtest.view.adapter.PhotoAdapter;
import com.leboncoin.steventbtest.view.component.DividerListView;
import com.leboncoin.steventbtest.view.listener.OnRecyclerViewClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Steven on 11/02/2018.
 */

public class PhotoListActivity extends BaseActivity implements PhotoListView, OnRecyclerViewClickListener {

    @BindView(R.id.act_photolist_cl)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.act_photolist_pb)
    ProgressBar progressBar;
    @BindView(R.id.act_photolist_layout_retry)
    LinearLayout layoutRetry;
    @BindView(R.id.act_photolist_bt_retry)
    Button btRetry;
    @BindView(R.id.act_photolist_rv)
    RecyclerView recyclerView;

    private PhotoListPresenter mPresenter;
    private PhotoAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photolist);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerListView(this, ContextCompat.getDrawable(this, R.drawable.divider_simple_line)));

        mAdapter = new PhotoAdapter(getApplicationContext(), this);
        recyclerView.setAdapter(mAdapter);

        if (mPresenter == null) {
            mPresenter = new PhotoListPresenter(this, BaseApplication.from(this).getRepository());
        }

        mPresenter.init();
    }

    @Override
    public void onRecyclerViewClick(Object clickedItem) {
        //Photo photo = (Photo) clickedItem;
        //TODO : call nav utils to go to another activity with details
    }

    @Override
    public void renderPhotoList(List<Photo> photoList) {
        if (null != photoList){
            mAdapter.setData(photoList);
        }
    }
    @OnClick(R.id.act_photolist_bt_retry)
    public void onClickRetry() {
        mPresenter.init();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRetryLayout() {
        layoutRetry.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideRetryLayout() {
        layoutRetry.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
}
