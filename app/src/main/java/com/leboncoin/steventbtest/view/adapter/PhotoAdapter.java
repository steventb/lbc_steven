package com.leboncoin.steventbtest.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leboncoin.steventbtest.model.entity.Photo;
import com.leboncoin.steventbtest.view.holder.PhotoHolder;
import com.leboncoin.steventbtest.view.listener.OnHolderClickListener;
import com.leboncoin.steventbtest.view.listener.OnRecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steven on 11/02/2018.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder> implements OnHolderClickListener {

    private Context mContext;
    private OnRecyclerViewClickListener mClickListener;
    private List<Photo> photoList;

    public PhotoAdapter(Context mContext, OnRecyclerViewClickListener mClickListener) {
        this.mContext = mContext;
        this.mClickListener = mClickListener;
        photoList = new ArrayList<>();
    }
    
    @Override
    public void onBindViewHolder(PhotoHolder holder, int position) {
        Photo photo = photoList.get(position);
        holder.render(mContext, position, photo);
    }

    @Override
    public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(PhotoHolder.getLayoutId(), parent, false);
        return new PhotoHolder(view, this);
    }

    @Override
    public int getItemCount() {
        return photoList == null ? 0 : photoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(int position) {
        mClickListener.onRecyclerViewClick(photoList.get(position));
    }

    public void setData(List<Photo> photoList){
        this.photoList = photoList;
        notifyDataSetChanged();
    }
}
