package com.leboncoin.steventbtest.view.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.leboncoin.steventbtest.R;

/**
 * Created by Steven on 13/02/2018.
 * A basic and quick view to render the list a quite more elegant
 */

public class DividerListView extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    private Context mContext;

    public DividerListView(Context context, Drawable divider) {
        mDivider = divider;
        mContext = context;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        final int HORIZONTAL_MARGIN = (int) mContext.getResources().getDimension(R.dimen.activity_horizontal_margin);
        final int left = parent.getPaddingLeft()
                + (int) mContext.getResources().getDimension(R.dimen.activity_list_icon_width)
                + 2 * HORIZONTAL_MARGIN;
        final int right = parent.getWidth() - parent.getPaddingRight() - HORIZONTAL_MARGIN;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}