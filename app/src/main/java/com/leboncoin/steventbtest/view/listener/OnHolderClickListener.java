package com.leboncoin.steventbtest.view.listener;

/**
 * Created by Steven on 11/02/2018.
 */

public interface OnHolderClickListener {
    void onClick(int position);
}
