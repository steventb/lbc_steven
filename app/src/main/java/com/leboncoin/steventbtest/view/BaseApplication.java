package com.leboncoin.steventbtest.view;

import android.app.Application;
import android.content.Context;

import com.leboncoin.steventbtest.model.Environment;
import com.leboncoin.steventbtest.model.network.Network;
import com.leboncoin.steventbtest.model.network.NetworkImpl;
import com.leboncoin.steventbtest.model.photo.PhotoClient;
import com.leboncoin.steventbtest.model.photo.PhotoRepository;

/**
 * Created by Steven on 11/02/2018.
 */

public class BaseApplication extends Application {

    private PhotoRepository mPhotoRepository;
    private PhotoClient mPhotoClient;
    private Network mNetwork;

    public static BaseApplication from(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public PhotoRepository getRepository() {
        if (mPhotoRepository == null) {
            mPhotoRepository = new PhotoRepository(getPhotoClient());
        }
        return mPhotoRepository;
    }

    public PhotoClient getPhotoClient() {
        if (mPhotoClient == null) {
            mPhotoClient = new PhotoClient(getNetwork());
        }
        return mPhotoClient;
    }

    public Network getNetwork() {
        if (mNetwork == null) {
            mNetwork = new NetworkImpl(this, Environment.ENDPOINT);
        }
        return mNetwork;
    }


}
