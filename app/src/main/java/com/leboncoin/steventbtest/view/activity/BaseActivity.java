package com.leboncoin.steventbtest.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.leboncoin.steventbtest.utils.Navigation;

/**
 * Created by Steven on 11/02/2018.
 */

public class BaseActivity extends AppCompatActivity {
    protected Navigation mNavigation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO : uncomment this when at least two activities are created
        //mNavigation = new Navigation();
    }
}
