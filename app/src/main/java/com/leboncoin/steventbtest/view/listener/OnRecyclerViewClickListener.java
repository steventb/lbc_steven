package com.leboncoin.steventbtest.view.listener;

/**
 * Created by Steven on 11/02/2018.
 */

public interface OnRecyclerViewClickListener {
    void onRecyclerViewClick(Object clickedItem);
}
