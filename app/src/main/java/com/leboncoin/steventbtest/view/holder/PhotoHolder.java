package com.leboncoin.steventbtest.view.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.leboncoin.steventbtest.R;
import com.leboncoin.steventbtest.model.entity.Photo;
import com.leboncoin.steventbtest.view.component.ImageComponent;
import com.leboncoin.steventbtest.view.listener.OnHolderClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Steven on 11/02/2018.
 */

public class PhotoHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.item_photo_thumb)
    ImageView icon;
    @BindView(R.id.item_photo_title)
    TextView title;

    private OnHolderClickListener holderClickListener;

    public PhotoHolder(View itemView, OnHolderClickListener clickListener) {
        super(itemView);
        holderClickListener = clickListener;
        ButterKnife.bind(this, itemView);
    }

    public static int getLayoutId(){
        return R.layout.item_photo;
    }

    public void render(Context context, int position, Photo photo) {
        ImageComponent.loadImage(context, photo.thumbUrl, icon);
        title.setText(photo.title);
    }
}
