package com.leboncoin.steventbtest.view;

import com.leboncoin.steventbtest.model.entity.Photo;

import java.util.List;

/**
 * Created by Steven on 11/02/2018.
 */

public interface PhotoListView{
    void renderPhotoList(List<Photo> photoList);
    void showRetryLayout();
    void hideRetryLayout();
    void showProgressBar();
    void hideProgressBar();
}
